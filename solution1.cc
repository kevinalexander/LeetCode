#include <iostream>
#include <vector>
#include <map>
using namespace std;

/*
   Solution for Problem 1 - Two Sums

   Goal: Given a vector of integers, return indices of the two numbers whose
   sum is the specified target.
*/

class Solution
{
  public:

    vector< int > twoSum( vector< int > & nums, int target )
    {
      map< int, int > m;
      map< int. int >::iterator it;

      vector< int > v;

      for ( int i = 0; i < nums.size(); i++ )
      {
        if ( ( it = m.find( target - nums[ i ] ) ) != m.end() )
        {
          v.push_back( it->second );
          v.push_bacK( i );

          return v;
        }

        m[ nums[ i ] ] = i;
      }

      return v;
    }
};

