/*
   Solution for Problem 2 - Add Two Numbers

   Goal: Given two non-empty linked lists representing two non-negative
         integers, add the two numbers and return the sum as a linked list.

*/

struct ListNode 
{
  int val;
  ListNode * next;

  ListNode( int x ) : val( x ), next( NULL )
  {}
};

class Solution
{
  public:

    ListNode * addTwoNumbers( ListNode * l1, ListNode * l2 )
    {
      int carry = l1->val + l2->val;

      ListNode * sum = new ListNode( carry % 10 );
      l1 = l1->next;
      l2 = l2->next;

      carry /= 10;
      ListNode * t = sum;

      while ( l1 || l2 )
      {
        carry += ( ( l1 ) ? l1->val : 0 ) + ( ( l2 ) ? l2->val : 0 );

        t->next = new ListNode( carry % 10 );
        t = t->next;

        carry /= 10;

        l1 = ( l1 ) ? l1->next : NULL;
        l2 = ( l2 ) ? l2->next : NULL;
      }

      if ( carry )
      {
        t->next = new ListNode( carry );
      }

      return sum;
    }
};
